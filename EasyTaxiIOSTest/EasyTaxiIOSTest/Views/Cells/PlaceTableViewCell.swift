//
//  PlaceTableViewCell.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 23/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ place: Address){
        if let formattedAddress = place.formatedAddress {
            addressLabel.text = formattedAddress
        }
    }
}
