//
//  ViewController.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 22/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import Toast_Swift
import Alamofire

enum ScreenStatus {
    case loadTaxi
    case searchingTaxi
    case taxiOnYourWay
}

class MapViewController: UIViewController {
    
    // MARK: Variables
    let locationManager = CLLocationManager()
    var places = [Address]()
    var taxis = [Taxi]()
    var markers = [GMSMarker]()
    var tableView: UITableView!
    var nearest: Taxi!
    var minorDistance: Double!
    var screenStatus: ScreenStatus = .loadTaxi {
        didSet {
            switch screenStatus {
            case .loadTaxi:
                updateTaxis(mapView.camera.target)
                mapView.isUserInteractionEnabled = true
                infoButton.setTitle(R.string.localizable.loadTaxis(), for: .normal)
                infoLabel.textAlignment = .left
                starButton.isHidden = false
                mapView.settings.myLocationButton = true
                break
            case .searchingTaxi:
                infoButton.setTitle(R.string.localizable.searchingForATaxi(), for: .normal)
                infoButton.loadingIndicator(true)
                mapView.isUserInteractionEnabled = false
                starButton.isHidden = true
                infoButton.isUserInteractionEnabled = false
                mapView.settings.myLocationButton = false
                break
            case .taxiOnYourWay:
                infoButton.setTitle(R.string.localizable.cancel(), for: .normal)
                infoButton.loadingIndicator(false)
                infoButton.isUserInteractionEnabled = true
                infoLabel.text = R.string.localizable.taxiFound(Int(minorDistance*10000))
                infoLabel.textAlignment = .center
                break
            }
        }
    }
    
    // MARK: Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBAction func infoButton(_ sender: UIButton) {
        switch screenStatus {
        case .loadTaxi:
            let _ = findNearestTaxi(mapView.camera.target)
            break
        case .taxiOnYourWay:
            screenStatus = .loadTaxi
            break
        default:
            print("No Action")
        }
    }
    
    // MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.requestLocation()
        
        infoButton.layer.cornerRadius = infoButton.frame.size.height/2
        
        let myMarker = UIImageView(image: R.image.ic_person_pin_circle_48pt())
        myMarker.frame = CGRect(origin: CGPoint(x: mapView.frame.size.width/2-myMarker.frame.size.width/2, y: mapView.frame.size.height/2-myMarker.frame.size.height), size: myMarker.frame.size)
        mapView.addSubview(myMarker)
        
        view.bringSubview(toFront: searchBar)
        for subview in searchBar.subviews.last!.subviews {
            if subview.isKind(of: NSClassFromString("UISearchBarBackground")!) {
                subview.alpha = 0.0
            }
        }
        searchBar.addToolBar(self)
    }
    
    // MARK: Class Methods
    func findNearestTaxi(_ myLocation:CLLocationCoordinate2D) -> Taxi{
        screenStatus = .searchingTaxi
        
        for taxi in taxis {
            let distance = (pow(myLocation.latitude-taxi.lat, 2.0)+pow(myLocation.longitude-taxi.lng, 2.0)).squareRoot()
            if nearest == nil {
                minorDistance = distance
                nearest = taxi
            }else if distance < minorDistance {
                minorDistance = distance
                nearest = taxi
            }
        }
        
        delay(delay: 3.0) {
            self.screenStatus = .taxiOnYourWay
            
            for marker in self.markers {
                marker.map = nil
            }
            
            self.markers = [GMSMarker]()
            
            let newMarker = GMSMarker(position: self.nearest.coordinate)
            newMarker.map = self.mapView
            newMarker.icon = R.image.ic_local_taxi()
            
            self.markers.append(newMarker)
        }
        
        return nearest
    }
    
    func showTableView(){
        if tableView == nil {
            tableView = UITableView(frame: CGRect(x: 8, y: view.frame.size.height, width: self.view.frame.width-16, height: self.view.frame.height))
            tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
            tableView.register(R.nib.placeTableViewCell(), forCellReuseIdentifier: "PLACE_CELL_ID")
            tableView.delegate = self
            tableView.dataSource = self
            tableView.layer.cornerRadius = 5
            self.view.addSubview(tableView)
            
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.frame = CGRect(x: 8, y: self.searchBar.frame.origin.y+self.searchBar.frame.size.height, width: self.view.frame.width-16, height: self.view.frame.height)
            })
        }
        tableView.reloadData()
    }
    
    func hideTableView(){
        if tableView != nil {
            UIView.animate(withDuration: 0.5, animations: {
                self.tableView.frame = CGRect(x: 8, y: self.view.frame.size.height, width: self.view.frame.width-16, height: self.view.frame.height)
            }, completion: { (success) in
                self.tableView.removeFromSuperview()
                self.tableView = nil
            })
        }
    }
    
    func showError(_ error: Error){
        if let error = error as? AFError {
            if error.responseCode! >= 400 {
                self.view.makeToast(R.string.localizable.otherError())
            }else{
                self.view.makeToast(R.string.localizable.noTaxisFound())
            }
        }
        
        if (error as? URLError) != nil {
            self.view.makeToast(R.string.localizable.connectionProblem())
        }
    }
    
    func updateTaxisMarkers(_ taxis:[Taxi]) {
        self.taxis = taxis
        
        for marker in markers{
            marker.map = nil
        }
        
        markers = [GMSMarker]()
        for taxi in taxis {
            let newMarker = GMSMarker(position: taxi.coordinate)
            newMarker.map = mapView
            newMarker.icon = R.image.ic_local_taxi()
            
            markers.append(newMarker)
        }
    }
    
    func updateAddress(_ coordinate: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (placemarks, error) in
            if let placemark = placemarks?.firstResult() {
                self.infoLabel.text = placemark.lines?[0]
            }else{
                self.view.makeToast(R.string.localizable.cannotGetLocation())
            }
        }
    }
    
    func updateTaxis(_ coordinate: CLLocationCoordinate2D){
        nearest = nil
        minorDistance = nil
        self.updateAddress(coordinate)
        Taxi.getTaxis(byLocation: coordinate, onSuccess: { (taxis) in
            self.updateTaxisMarkers(taxis)
        }) { (error) in
            self.showError(error)
        }
    }
}

// MARK: UITableViewDataSource
extension MapViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PLACE_CELL_ID", for: indexPath) as! PlaceTableViewCell
        
        cell.setData(places[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

// MARK: UITableViewDelegate
extension MapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = places[indexPath.row]
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        hideTableView()
        let coordinates = CLLocationCoordinate2DMake(place.lat, place.lng)
        mapView.moveCamera(GMSCameraUpdate.setTarget(coordinates))
    }
}

// MARK: UISearchBarDelegate
extension MapViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            places.removeAll()
            hideTableView()
        }else{
            
            Address.searchAddress(byAddress: searchText, onSuccess: { (address) in
                self.places = address
                self.showTableView()
                self.tableView.reloadData()
            }, onFailure: { (error) in
                self.view.makeToast(error.localizedDescription)
            })
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        hideTableView()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        hideTableView()
    }
}

// MARK: CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let coordinate = locationManager.location?.coordinate {
            let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 17)
            mapView.camera = camera
            mapView.settings.compassButton = true
            mapView.delegate = self
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        view.makeToast(R.string.localizable.cannotGetLocation())
    }
}

// MARK: GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        updateTaxis(position.target)
    }
}
