//
//  File.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 23/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

struct Address {
    var formatedAddress: String!
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    var coordinate: CLLocationCoordinate2D! {
        get{
            return CLLocationCoordinate2DMake(lat, lng)
        }
    }
}

extension Address {
    static func searchAddress(byAddress address: String, onSuccess: @escaping ([Address]) -> Void, onFailure: @escaping (Error!) -> Void){
        APIClient.shared.searchAddress(address, onSuccess: onSuccess, onFailure: onFailure)
    }
}

extension Address: Mappable {
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        formatedAddress     <- map["formatted_address"]
        lat                 <- map["geometry.location.lat"]
        lng                 <- map["geometry.location.lng"]
    }
}
