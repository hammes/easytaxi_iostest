//
//  File.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 23/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

struct Taxi {
    var driverName: String!
    var driverCar: String!
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    var coordinate: CLLocationCoordinate2D! {
        get{
            return CLLocationCoordinate2DMake(lat, lng)
        }
    }
}

extension Taxi {
    static func getTaxis(byLocation location:CLLocationCoordinate2D, onSuccess: @escaping ([Taxi]) -> Void, onFailure: @escaping (Error!) -> Void){
        APIClient.shared.getTaxis(location.latitude, lng: location.longitude, onSuccess: onSuccess, onFailure: onFailure)
    }
}

extension Taxi: Mappable {
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        driverName      <- map["driver-name"]
        driverCar       <- map["driver-car"]
        lat             <- map["lat"]
        lng             <- map["lng"]
    }
}
