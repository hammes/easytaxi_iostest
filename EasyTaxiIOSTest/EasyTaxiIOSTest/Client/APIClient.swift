//
//  APIClient.swift
//  
//
//  Created by Marcelo Hammes on 02/08/16.
//
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class APIClient: NSObject {
    static let shared = APIClient()
    
    func getTaxis(_ lat: Double, lng: Double, onSuccess: @escaping ([Taxi]) -> Void, onFailure: @escaping (Error!) -> Void){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(API_BASE_URL+"gettaxis", parameters: ["lat":lat, "lng":lng])
            .validate()
            .responseArray(keyPath: "taxis"){ (response: DataResponse<[Taxi]>) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                switch response.result {
                case .success:
                    if let value = response.result.value{
                        onSuccess(value)
                    }else{
                        onFailure(response.result.error)
                    }
                case .failure(let error):
                    onFailure(error)
                }
        }
    }
    
    func searchAddress(_ address: String, onSuccess: @escaping ([Address]) -> Void, onFailure: @escaping (Error!) -> Void){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let encondedAddress = address.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        
        Alamofire.request(GOOGLE_MAPS_BASE_PATH, parameters: ["address":encondedAddress ?? "", "key":"AIzaSyD8ET_e6FqlmqqgFCPnuuZvSiAcMoX1nY4"])
            .validate()
            .responseArray(keyPath: "results"){ (response: DataResponse<[Address]>) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                switch response.result {
                case .success:
                    if let value = response.result.value{
                        onSuccess(value)
                    }else{
                        onFailure(response.result.error)
                    }
                case .failure(let error):
                    onFailure(error)
                }
        }
    }
}
