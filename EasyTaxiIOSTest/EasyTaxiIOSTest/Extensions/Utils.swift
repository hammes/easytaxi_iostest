//
//  Utils.swift
//  Assai2
//
//  Created by Marcelo Hammes on 15/08/16.
//  Copyright © 2016 Onesight. All rights reserved.
//

import Foundation

func delay(delay:Double, closure:@escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
}
