//
//  UIButton.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 24/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import UIKit

extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 84561
        if show {
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            indicator.center = CGPoint(x: 32, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
