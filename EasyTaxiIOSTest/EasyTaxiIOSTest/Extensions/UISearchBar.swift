//
//  UITextField.swift
//  CompraLocalVendedor
//
//  Created by Onesight on 15/05/17.
//  Copyright © 2017 Onesight. All rights reserved.
//

import UIKit

extension UISearchBar {
    func addToolBar(_ delegate: UISearchBarDelegate!){
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: R.string.localizable.closeKeyboard(), style: .done, target: self, action: #selector(cancelPressed))
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        self.delegate = delegate
        
        inputAccessoryView = toolBar
    }
    func cancelPressed(){
        endEditing(true)
        self.text = ""
    }
}
