//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    func substring(_ from: Int) -> String {
        return substring(from: characters.index(startIndex, offsetBy: from))
    }
    
    var length: Int {
        return characters.count
    }
}
