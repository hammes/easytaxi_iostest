//
//  ReusableViewExtension.swift
//  CompraLocalVendedor
//
//  Created by Onesight on 12/04/17.
//  Copyright © 2017 Onesight. All rights reserved.
//

import UIKit

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
