//
//  NibLoadableViewExtension.swift
//  CompraLocalVendedor
//
//  Created by Onesight on 12/04/17.
//  Copyright © 2017 Onesight. All rights reserved.
//

import UIKit

extension NibLoadableView where Self: UIView {
    
    static var NibName: String {
        return String(describing: self)
    }
}
