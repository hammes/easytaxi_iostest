//
//  ViewProtocols.swift
//  CompraLocalVendedor
//
//  Created by Onesight on 12/04/17.
//  Copyright © 2017 Onesight. All rights reserved.
//

import Foundation

protocol ReusableView: class {}

protocol NibLoadableView: class { }
