//
//  TaxiTests.swift
//  EasyTaxiIOSTest
//
//  Created by Onesight on 24/08/17.
//  Copyright © 2017 Hammes. All rights reserved.
//

import XCTest
import CoreLocation
@testable import EasyTaxiIOSTest

class TaxiTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetTaxis() {
        let expectat = expectation(description: "getTaxi")
        
        Taxi.getTaxis(byLocation: CLLocationCoordinate2DMake(-23.542284036008898, -46.730822985635804), onSuccess: { (taxis) in
            XCTAssertTrue(taxis.count > 0)
            expectat.fulfill()
        }) { (error) in
            XCTFail()
            expectat.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPerformanceGetTaxis() {
        self.measure {
            Taxi.getTaxis(byLocation: CLLocationCoordinate2DMake(-23.542284036008898, -46.730822985635804), onSuccess: { (taxis) in
                self.stopMeasuring()
            }) { (error) in
                self.stopMeasuring()
            }
        }
    }
    
}
